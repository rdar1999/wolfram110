#include "mainwindow.h"

static int counter = 0;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    setupUi(this);

    auto openGL = new GLWidget(&w110, this);
    openGL->setGeometry(10,40,800,600);

    connect(timer, &QTimer::timeout, openGL, &GLWidget::animate);

    openGL->installEventFilter(this);

    w110.init();
}

void MainWindow::on_pushButton_clicked()
{
    ++counter;
    if (counter%2 ==1){
        timer->start(5);
        pushButton->setText("Pause Simulation");
    } else{
        timer->stop();
        pushButton->setText("Resume Simulation");
    }
}
