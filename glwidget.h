#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QOpenGLWidget>
#include "wolfram.h"

class wolfram;

class GLWidget : public QOpenGLWidget
{
    Q_OBJECT

public:
    GLWidget(wolfram *w, QWidget *parent);
    ~GLWidget() override;

public slots:
    void animate();

protected:
    void paintEvent(QPaintEvent *event) override;

private:
    wolfram *w110;
    int elapsed;
};

#endif // GLWIDGET_H
