#include "wolfram.h"
#include <QPainter>
#include <QPaintEvent>
#include <random>


wolfram::wolfram()
    : grid(gridSize.x, std::vector <int>(gridSize.y, 0)),
      image(QSize(gridSize.x, gridSize.y), QImage::Format_RGB32),
      buffer(image)
{

}

void wolfram::init()
{
    std::random_device r;
    std::mt19937 gen(r());
    std::uniform_int_distribution<unsigned char> dis{};

    for (auto i = 0ul; i < grid.size(); ++i)
    {
        grid[i][0] = static_cast<int>( dis(gen) < 192 );
    }
}

int wolfram::evaluateCell(unsigned long posX, unsigned long posY)
{
    auto x = gridSize.x;  auto y = gridSize.y;

    auto a = grid[(posX+x-1)%x][(posY+y-1)%y];
    auto b = grid[(posX+x  )%x][(posY+y-1)%y];
    auto c = grid[(posX+x+1)%x][(posY+y-1)%y];

    return (a&b&c) ^ (b&c) ^ (b) ^ (c);
}

void wolfram::paint(QPainter *painter, QPaintEvent *event)
{
    painter->fillRect(event->rect(), Qt::transparent);

    int t = counter >= (gridSize.y - 1);

    QPainter paintbuff;

    paintbuff.begin(&image);

    paintbuff.drawImage(0,0,buffer.copy(0, t, gridSize.x, gridSize.y-t));

    paintbuff.end();

    std::vector< std::vector <int> > aux (gridSize.x, std::vector <int>(gridSize.y, 0));

    if (t){
        for (int i = 0; i < gridSize.x; ++i) {
            grid[i].erase(grid[i].begin());
        }
    }

    counter = counter + !t;

    for (auto i = 0ul; i < grid.size(); ++i)
    {
        aux[i][counter] = evaluateCell(i,counter);
        image.setPixel(i, counter, qRgb(0,255*aux[i][counter], 0) );
    }

    painter->drawImage(event->rect(), image);

    buffer = image;

    image.fill(Qt::transparent);

    grid.clear();
    grid = aux;
}
