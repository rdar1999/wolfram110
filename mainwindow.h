#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ui_mainwindow.h"
#include "glwidget.h"
#include <QTimer>

class MainWindow : public QMainWindow, private Ui::MainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    QTimer *timer = new QTimer(this);
    wolfram w110 = *new wolfram();

private slots:
    void on_pushButton_clicked();
};

#endif // MAINWINDOW_H
