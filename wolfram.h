#ifndef WOLFRAM_H
#define WOLFRAM_H

#include <QImage>
#include <QWidget>


class wolfram
{
public:
    wolfram();

    struct gridSize
    {
        unsigned long x;
        unsigned long y;
        gridSize(){x=800; y=600;}
    } gridSize;

    void init();
    int evaluateCell(unsigned long posX, unsigned long posY);
    void paint(QPainter *painter, QPaintEvent *event);
    std::vector< std::vector <int> > grid;
private:
    unsigned counter=0;
    QImage image;
    QImage buffer;

};

#endif // WOLFRAM_H
