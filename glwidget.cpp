#include "glwidget.h"

#include <QPainter>
#include <QTimer>

GLWidget::GLWidget(wolfram *w, QWidget *parent)
    : QOpenGLWidget(parent), w110(w)
{
    elapsed = 0;
    setFixedSize(w->gridSize.x, w->gridSize.y);
    setAutoFillBackground(false);

    glEnable(GL_MULTISAMPLE);
}

GLWidget::~GLWidget()
{
    delete this;
}

void GLWidget::animate()
{
    update();
}

void GLWidget::paintEvent(QPaintEvent *event)
{
    QPainter painter;
    painter.begin(this);
    painter.setRenderHint(QPainter::Antialiasing);
    w110->paint(&painter, event);
    painter.end();
}


